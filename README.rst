Json-Parser
===========

Json-Parser is an implementation to parse json from scratch.

Install
-------

.. code-block:: bash

    $ git clone https://mohan3d@bitbucket.org/mohan3d/json-parser.git
    $ cd json-parser
    $ python main.py


Usage
-----

.. note:: Json-Parser is not suitable for production or real usage, use it for educational purpose only.


.. code-block:: python

    import parser
    data = parser.loads(json_str)
    print(data)

Testing
-------

.. code-block:: bash

    $ cd json-parser
    $ python -m unittest tests/*.py
