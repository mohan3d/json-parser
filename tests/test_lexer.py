import unittest

from parser import (
    Lexer, LCB, RCB, LSB, RSB, COMMA, COLON, PERIOD, E, PLUS, MINUS, STRING, INTEGER, TRUE, FALSE, NULL, EOF
)


class TestLexer(unittest.TestCase):
    def test_curly_brackets(self):
        lexer = Lexer("{        }")
        self.assertEqual(lexer.get_next_token().type, LCB)
        self.assertEqual(lexer.get_next_token().type, RCB)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_square_brackets(self):
        lexer = Lexer("[ ]")
        self.assertEqual(lexer.get_next_token().type, LSB)
        self.assertEqual(lexer.get_next_token().type, RSB)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_comma(self):
        lexer = Lexer("  , ")
        self.assertEqual(lexer.get_next_token().type, COMMA)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_colon(self):
        lexer = Lexer("::")
        self.assertEqual(lexer.get_next_token().type, COLON)
        self.assertEqual(lexer.get_next_token().type, COLON)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_period(self):
        lexer = Lexer(".    .")
        self.assertEqual(lexer.get_next_token().type, PERIOD)
        self.assertEqual(lexer.get_next_token().type, PERIOD)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_plus(self):
        lexer = Lexer("+  ")
        self.assertEqual(lexer.get_next_token().type, PLUS)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_minus(self):
        lexer = Lexer("-")
        self.assertEqual(lexer.get_next_token().type, MINUS)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_e_case_1(self):
        lexer = Lexer("2e5")
        self.assertEqual(lexer.get_next_token().type, INTEGER)
        self.assertEqual(lexer.get_next_token().type, E)
        self.assertEqual(lexer.get_next_token().type, INTEGER)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_e_case_2(self):
        lexer = Lexer("3e+3")
        self.assertEqual(lexer.get_next_token().type, INTEGER)
        self.assertEqual(lexer.get_next_token().type, E)
        self.assertEqual(lexer.get_next_token().type, PLUS)
        self.assertEqual(lexer.get_next_token().type, INTEGER)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_e_case_3(self):
        lexer = Lexer("5e-2")
        self.assertEqual(lexer.get_next_token().type, INTEGER)
        self.assertEqual(lexer.get_next_token().type, E)
        self.assertEqual(lexer.get_next_token().type, MINUS)
        self.assertEqual(lexer.get_next_token().type, INTEGER)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_eof(self):
        lexer = Lexer("   ")
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_true(self):
        lexer = Lexer("true")
        self.assertEqual(lexer.get_next_token().type, TRUE)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_false(self):
        lexer = Lexer("false")
        self.assertEqual(lexer.get_next_token().type, FALSE)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_null(self):
        lexer = Lexer("null")
        self.assertEqual(lexer.get_next_token().type, NULL)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_integer_case_1(self):
        lexer = Lexer("1234")
        self.assertEqual(lexer.get_next_token().type, INTEGER)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_integer_case_2(self):
        lexer = Lexer("[1, 2, 3, 4, 5]")
        self.assertEqual(lexer.get_next_token().type, LSB)

        for i in range(4):
            self.assertEqual(lexer.get_next_token().type, INTEGER)
            self.assertEqual(lexer.get_next_token().type, COMMA)

        self.assertEqual(lexer.get_next_token().type, INTEGER)
        self.assertEqual(lexer.get_next_token().type, RSB)

    def test_string_case_1(self):
        lexer = Lexer('{"Key": "Value"}')
        self.assertEqual(lexer.get_next_token().type, LCB)
        self.assertEqual(lexer.get_next_token().type, STRING)
        self.assertEqual(lexer.get_next_token().type, COLON)
        self.assertEqual(lexer.get_next_token().type, STRING)
        self.assertEqual(lexer.get_next_token().type, RCB)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_string_case_2(self):
        n = 4
        lexer = Lexer('["Val1", "Val2", "Val3", "Val4"]')
        self.assertEqual(lexer.get_next_token().type, LSB)

        for i in range(n - 1):
            self.assertEqual(lexer.get_next_token().type, STRING)
            self.assertEqual(lexer.get_next_token().type, COMMA)

        self.assertEqual(lexer.get_next_token().type, STRING)

        self.assertEqual(lexer.get_next_token().type, RSB)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_mixed_case_1(self):
        text = "{[  . ,     : ],}"
        tokens_types = [
            LCB,
            LSB,
            PERIOD,
            COMMA,
            COLON,
            RSB,
            COMMA,
            RCB
        ]

        lexer = Lexer(text)

        for ttype in tokens_types:
            self.assertEqual(lexer.get_next_token().type, ttype)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_mixed_case_2(self):
        text = "[null, true, false, \"string\"]"
        lexer = Lexer(text)

        tokens_types = [
            LSB,
            NULL,
            COMMA,
            TRUE,
            COMMA,
            FALSE,
            COMMA,
            STRING,
            RSB,
        ]

        for ttype in tokens_types:
            self.assertEqual(lexer.get_next_token().type, ttype)
        self.assertEqual(lexer.get_next_token().type, EOF)

    def test_unrecognized(self):
        lexer = Lexer("\\")
        with self.assertRaises(Exception):
            lexer.get_next_token()


if __name__ == '__main__':
    unittest.main()
