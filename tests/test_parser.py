import json
import unittest

from parser import Lexer, Parser


def create_parser(s):
    lexer = Lexer(s)
    return Parser(lexer)


class TestParser(unittest.TestCase):
    def test_empty_object(self):
        parser = create_parser("{}")
        self.assertEqual(parser.object_(), {})

    def test_object(self):
        parser = create_parser('{"k": "v", "1":"1"}')
        self.assertEqual(parser.object_(), {"k": "v", "1": "1"})

    def test_empty_array(self):
        parser = create_parser("[]")
        self.assertEqual(parser.array_(), [])

    def test_array(self):
        parser = create_parser('["1", "x", {}, []]')
        self.assertEqual(parser.array_(), ["1", "x", {}, []])

    def test_empty_string(self):
        parser = create_parser('""')
        self.assertEqual(parser.string_(), "")

    def test_string(self):
        parser = create_parser('"abc"')
        self.assertEqual(parser.string_(), "abc")

    def test_pair(self):
        parser = create_parser('"key": "val"')
        self.assertEqual(parser.pair(), ("key", "val"))

    def test_members(self):
        parser = create_parser('"k1": "v1",   "k2": [], "k3": {}')
        self.assertEqual(parser.members(), [("k1", "v1"), ("k2", []), ("k3", {})])

    def test_value_true(self):
        parser = create_parser("true")
        self.assertEqual(parser.value(), True)

    def test_value_false(self):
        parser = create_parser("false")
        self.assertEqual(parser.value(), False)

    def test_value_null(self):
        parser = create_parser("null")
        self.assertEqual(parser.value(), None)

    def test_value_object(self):
        parser = create_parser('{"k": "v"}')
        self.assertEqual(parser.value(), {"k": "v"})

    def test_value_array(self):
        parser = create_parser('["x", "y", "z"]')
        self.assertEqual(parser.value(), ["x", "y", "z"])

    def test_value_string(self):
        parser = create_parser('"string"')
        self.assertEqual(parser.value(), "string")

    def test_fraction(self):
        parser = create_parser('.123456789')
        self.assertEqual(parser.fraction(), 123456789)

    def test_e(self):
        cases = ['e5', 'e+5', 'e-5']
        values = [1, 1, -1]

        for case, value in zip(cases, values):
            parser = create_parser(case)
            self.assertEqual(parser.e(), value)

    def test_exp(self):
        cases = ['e6', 'e+51', 'e-15']
        values = [6, 51, -15]

        for case, value in zip(cases, values):
            parser = create_parser(case)
            self.assertEqual(parser.exp(), value)

    def test_integer(self):
        cases = [1, 2, -3, -4, 5]

        for case in cases:
            parser = create_parser(str(case))
            self.assertEqual(parser.integer(), case)

    def test_value_number(self):
        cases = ['1', '123', '-20', '-5', '1.2',
                 '23.234234', '2e5', '3e+5', '2e-3',
                 '2.3e4', '1.2e-5', '10.3e2', '4.5e+2']

        for case in cases:
            parser = create_parser(str(case))
            self.assertEqual(parser.number(), float(case))

    def test_parse(self):
        data = {
            'arr': ["a", "b", "c", ["x", "y", "z"], {'x': 'y'}],
            'nums': {
                str(i): i for i in range(5)
            },
            'obj': {
                'x': {
                    '1': '1'
                },
                'y': [[], [], {}, 1, 2.3, True, None, False]
            },
            'str': 'abc',
            'true': True,
            'false': False,
            'null': None,
            'a': 1.5,
            'b': 2.333
        }

        text = json.dumps(data)
        parser = create_parser(text)
        self.assertEqual(parser.parse(), data)


if __name__ == '__main__':
    unittest.main()
