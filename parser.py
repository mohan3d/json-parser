from __future__ import print_function

LCB, RCB, LSB, RSB, COMMA, COLON, PERIOD, PLUS, MINUS, E, STRING, INTEGER, NUMBER, TRUE, FALSE, NULL, EOF = (
    '{', '}', '[', ']', ',', ':', '.', 'PLUS', 'MINUS', 'E', 'STRING', 'INTEGER', 'NUMBER', 'TRUE', 'FALSE', 'NULL',
    'EOF'
)


# ----------------- Lexer -----------------
class Token:
    def __init__(self, _type, value):
        self.type = _type
        self.value = value

    def __str__(self):
        return "Token({}, {})".format(self.type, self.value)


class Lexer:
    def __init__(self, text):
        self.text = text
        self.pos = 0
        self.current_char = text[0]

    def error(self):
        raise Exception('Invalid character')

    def advance(self):
        """Advances one char each time, and check for the EOF"""
        self.pos += 1

        if self.pos >= len(self.text):
            self.current_char = None
        else:
            self.current_char = self.text[self.pos]

    def multi_advance(self, n):
        """Advances n times"""
        for i in range(n):
            self.advance()

    def peek(self, s):
        """Checks if the next n chars forms the input s but don't advance"""
        n = len(s)
        pos = self.pos

        if n + pos > len(self.text):
            return False

        for i in range(n):
            if s[i] != self.text[i + pos]:
                return False

        return True

    def skip_whitespaces(self):
        """Skips all white spaces"""
        while self.current_char is not None and self.current_char.isspace():
            self.advance()

    def get_string(self):
        """Reads n chars till forms a valid string, stops if \\ or \" encountered"""
        s = []

        while self.current_char is not None and self.current_char not in ['"', '\\']:
            s.append(self.current_char)
            self.advance()

        return ''.join(s)

    def get_integer(self):
        """Reads n digit-chars returns int value"""
        s = []

        while self.current_char is not None and self.current_char.isdigit():
            s.append(self.current_char)
            self.advance()

        return int(''.join(s))

    def get_next_token(self):
        """Checks for all recognized tokens, raises error if it didn't recognize any valid token"""
        while self.current_char is not None:

            if self.current_char.isspace():
                self.skip_whitespaces()
                continue

            if self.current_char.isdigit():
                return Token(INTEGER, self.get_integer())

            if self.current_char == '+':
                self.advance()
                return Token(PLUS, '+')

            if self.current_char == '-':
                self.advance()
                return Token(MINUS, '-')

            if self.current_char in ('e', 'E'):
                if self.pos + 1 < len(self.text):
                    next_char = self.text[self.pos + 1]
                    if next_char.isdigit() or next_char in ('+', '-'):
                        self.advance()
                        return Token(E, 'e')

            if self.current_char == '{':
                self.advance()
                return Token(LCB, '{')

            if self.current_char == '}':
                self.advance()
                return Token(RCB, '}')

            if self.current_char == '[':
                self.advance()
                return Token(LSB, '[')

            if self.current_char == ']':
                self.advance()
                return Token(RSB, ']')

            if self.current_char == ',':
                self.advance()
                return Token(COMMA, ',')

            if self.current_char == ':':
                self.advance()
                return Token(COLON, ':')

            if self.current_char == '.':
                self.advance()
                return Token(PERIOD, '.')

            if self.current_char == '"':
                self.advance()
                t = Token(STRING, self.get_string())

                if self.current_char != "\"":
                    self.error()
                self.advance()

                return t

            if self.peek('true'):
                self.multi_advance(4)
                return Token(TRUE, 'true')

            if self.peek('false'):
                self.multi_advance(5)
                return Token(FALSE, 'false')

            if self.peek('null'):
                self.multi_advance(4)
                return Token(NULL, 'null')

            self.error()

        return Token(EOF, None)


# ----------------- Parser -----------------
class Parser:
    def __init__(self, lexer):
        self.lexer = lexer
        self.current_token = lexer.get_next_token()

    def error(self):
        raise Exception('Invalid syntax')

    def eat(self, token_type):
        """Checks for the current token type, raises error if it don't match token_type"""
        if self.current_token.type == token_type:
            self.current_token = self.lexer.get_next_token()
        else:
            self.error()

    def object_(self):
        """
        object : {}
               | { members }
        """
        members = []

        self.eat(LCB)

        if self.current_token.type != RCB:
            members = self.members()

        self.eat(RCB)

        return dict(members)

    def string_(self):
        s = self.current_token.value
        self.eat(STRING)
        return s

    def digits(self):
        val = self.current_token.value
        self.eat(INTEGER)
        return val

    def fraction(self):
        """
        fraction : . digits
        """
        self.eat(PERIOD)
        return self.digits()

    def e(self):
        """
        e : e
          | e+
          | e-
          | E
          | E+
          | E-
        """
        if self.current_token.type == E:
            self.eat(E)

            if self.current_token.type == PLUS:
                self.eat(PLUS)
                return 1
            elif self.current_token.type == MINUS:
                self.eat(MINUS)
                return -1

            return 1

    def exp(self):
        """
        exp : e digits
        """
        return self.e() * self.digits()

    def integer(self):
        if self.current_token.type == MINUS:
            self.eat(MINUS)
            return -self.digits()

        return self.digits()

    def number(self):
        """
        number : integer
               | integer fraction
               | integer exp
               | integer fraction exp
        """
        integer = self.integer()

        if self.current_token.type == PERIOD:
            fraction = self.fraction()

            if self.current_token.type != E:
                # integer fraction
                return float("{integer}.{fraction}".format(integer=integer,
                                                           fraction=fraction))
            else:
                # integer fraction exp
                return float("{integer}.{fraction}e{digits}".format(integer=integer,
                                                                    fraction=fraction,
                                                                    digits=self.e() * self.digits()))
        if self.current_token.type == E:
            # integer exp
            # self.eat(E)
            return float("{integer}e{digits}".format(integer=integer,
                                                     digits=self.e() * self.digits()))

        return integer

    def members(self):
        """
        members : pair
                | pair , members

        members : (pair)+
        """
        pairs = [self.pair()]

        while self.current_token.type == COMMA:
            self.eat(COMMA)
            pairs.append(self.pair())

        return pairs

    def pair(self):
        """pair : string : value"""
        key = self.string_()
        self.eat(COLON)
        value = self.value()

        return key, value

    def array_(self):
        elements = []

        self.eat(LSB)
        if self.current_token.type != RSB:
            elements = self.elements()

        self.eat(RSB)

        return elements

    def elements(self):
        """
        elements : value
                 | value , elements

        elements : (value)+
        """
        values = [self.value()]

        while self.current_token.type == COMMA:
            self.eat(COMMA)
            values.append(self.value())

        return values

    def value(self):
        """
        value : string
              | number
              | object
              | array
              | true
              | false
              | null
        """
        current_token_type = self.current_token.type

        if current_token_type == STRING:
            return self.string_()
        if current_token_type == INTEGER or current_token_type == MINUS:
            return self.number()
        if current_token_type == LCB:
            # Object
            return self.object_()
        if current_token_type == LSB:
            # Array
            return self.array_()
        if current_token_type == TRUE:
            self.eat(TRUE)
            return True
        if current_token_type == FALSE:
            self.eat(FALSE)
            return False
        if current_token_type == NULL:
            self.eat(NULL)
            return None

    def parse(self):
        """Parses input tokens, raises error if EOF is not reached"""
        value = self.value()

        if self.current_token.type != EOF:
            self.error()

        return value


def loads(s):
    """Parses json string"""
    lexer = Lexer(s)
    parser = Parser(lexer)
    return parser.parse()
