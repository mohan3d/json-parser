import json

import parser


def main():
    # Apixu json response
    # https://www.apixu.com/
    data = {
        "location": {
            "name": "Cairo",
            "region": "Al Qahirah",
            "country": "Egypt",
            "lat": 123,
            "lon": 456,
            "tz_id": "Africa/Cairo",
            "localtime_epoch": 1508932421,
            "localtime": "2017-10-25 13:53"
        },
        "current": {
            "last_updated_epoch": 1508931910,
            "last_updated": "2017-10-25 13:45",
            "temp_c": 18.0,
            "temp_f": 95.8,
            "is_day": 1,
            "condition": {
                "text": "Sunny",
                "icon": "//cdn.apixu.com/weather/64x64/day/113.png",
                "code": 1000
            },
            "wind_mph": 0.0,
            "wind_kph": 0.0,
            "wind_degree": 0,
            "wind_dir": "N",
            "pressure_mb": 1015.0,
            "pressure_in": 30.4,
            "precip_mm": 0.0,
            "precip_in": 0.0,
            "humidity": 24,
            "cloud": 0,
            "feelslike_c": 29.2,
            "feelslike_f": 84.6,
            "vis_km": 10.0,
            "vis_miles": 6.0
        }
    }

    json_str = json.dumps(data)
    loaded_data = parser.loads(json_str)
    print(loaded_data.keys() == data.keys())


if __name__ == '__main__':
    main()
